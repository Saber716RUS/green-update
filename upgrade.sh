#!/usr/bin/env bash

#update_v211

sudo apt remove -y firefox libreoffice rhythmbox celluloid transmission-common transmission-gtk mintupgrade mint-upgrade-info mintwelcome && sudo apt purge -y firefox libreoffice rhythmbox celluloid transmission-common transmission-gtk mintupgrade mint-upgrade-info mintwelcome && sudo apt autoremove -y

sudo apt purge -y hexchat hexchat-common hunspell-de-at-frami hunspell-de-ch-frami hunspell-de-de-frami hunspell-en-au hunspell-en-ca hunspell-en-gb hunspell-en-za hunspell-es hunspell-fr hunspell-fr-classical hunspell-it hunspell-pt-br hunspell-pt-pt hyphen-de hyphen-en-ca hyphen-en-gb hyphen-fr hyphen-it hyphen-pt-br hyphen-pt-pt libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za libreoffice-l10n-es libreoffice-l10n-fr libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw librhythmbox-core10 mythes-de mythes-de-ch mythes-en-au mythes-fr mythes-it mythes-pt-pt

sudo apt purge -y fonts-beng-extra fonts-deva-extra fonts-gargi fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-kacst* fonts-kalapi fonts-khmeros* fonts-lao fonts-lklug-sinhala fonts-lohit* fonts-nakula fonts-nanum* fonts-navilu fonts-noto-cjk fonts-noto-mono fonts-orya-extra fonts-pagul fonts-sahadeva fonts-samyak* fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-smc* fonts-takao* fonts-telu* fonts-tibetan-machine fonts-tlwg* fonts-wqy* fonts-yrsa-rasa

sudo dpkg --add-architecture i386
sudo add-apt-repository multiverse
sudo apt update && sudo apt upgrade -y
sudo apt install -y vlc audacious qbittorrent libnss3 icoutils libvulkan1 libvulkan1:i386 cabextract bc libgl1-mesa-glx libgl1-mesa-glx:i386 gamemode libjpeg62:i386 libcupsimage2:i386

sudo apt update && sudo apt upgrade -y
sudo apt full-upgrade -y
sudo sed -i 's/vanessa/vera/g' /etc/apt/sources.list.d/official-package-repositories.list
sudo apt update
sudo apt full-upgrade -y

sudo sed -i "s/3/2/" /etc/NetworkManager/conf.d/default-wifi-p
echo "X-GNOME-Autostart-enabled=false" >> /etc/xdg/autostart/geoclue-demo-agent.desktop
echo "X-GNOME-Autostart-enabled=false" >> /etc/xdg/autostart/mintreport.desktop
echo "X-GNOME-Autostart-enabled=false" >> /etc/xdg/autostart/mintupdate.desktop
echo "X-GNOME-Autostart-enabled=false" >> /etc/xdg/autostart/mintwelcome.desktop
echo "X-GNOME-Autostart-enabled=false" >> /etc/xdg/autostart/warpinator-autostart.desktop

sudo dpkg-reconfigure ca-certificates

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/sysctl.conf && sudo cp -r /tmp/sysctl.conf /etc/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/victoria.zip && unzip victoria.zip && sudo cp -r /tmp/victoria /usr/share/mintsources/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/linuxmint.list && sudo cp -r /tmp/linuxmint.list /usr/share/mint-mirrors && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/lsb-release && sudo cp -r /tmp/lsb-release /etc/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/greenlinux.xml && sudo cp -r /tmp/greenlinux.xml /usr/share/cinnamon-background-properties/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/mint-artwork.gschema.override && sudo cp -r /tmp/mint-artwork.gschema.override /usr/share/glib-2.0/schemas/ && cd ~
sudo service keyboard-setup restart
sudo udevadm trigger --subsystem-match=input --action=change

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/timesyncd.conf && sudo cp -r /tmp/timesyncd.conf /etc/systemd && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/grub && sudo cp -r /tmp/grub /etc/default/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/20-intel.conf && sudo cp -r /tmp/20-intel.conf /etc/X11/xorg.conf.d/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/truetype.zip && unzip truetype.zip && sudo cp -r /tmp/truetype /usr/share/fonts/ && cd ~
fc-cache -f -v

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/info && sudo cp -r /tmp/info /etc/linuxmint/ && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/mint-logo.zip && unzip mint-logo.zip && sudo rm -rf /usr/share/plymouth/themes/mint-logo && sudo cp -r /tmp/mint-logo /usr/share/plymouth/themes/ && cd ~
sudo update-initramfs -u

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/linuxmint-logo-ring-symbolic.svg && sudo cp -r /tmp/linuxmint-logo-ring-symbolic.svg /usr/share/icons/hicolor/scalable/apps && cd ~

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/neofetch_logo.zip && unzip neofetch_logo.zip && sudo cp -r /tmp/neofetch_logo /usr && cd ~ && neofetch && sudo cp -r /usr/neofetch_logo/config.conf ~/.config/neofetch/config.conf && neofetch

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/greenlinux.zip && unzip greenlinux.zip && sudo cp -r /tmp/greenlinux/ /usr/share/backgrounds/ && cd ~
DESKTOP_SESSION=$(env | grep DESKTOP_SESSION= | cut -d'=' -f2)
changeWallpaper () {
    case "$DESKTOP_SESSION" in
    "cinnamon")
    # Изменение для Cinnamon
    gsettings set org.cinnamon.desktop.background picture-uri "file:///usr/share/backgrounds/greenlinux/greenlinux.jpg"
    ;;
    "mate")
    # Изменение для MATE
    mateconftool-2 -t string --set /desktop/mate/background/picture_filename "/usr/share/backgrounds/greenlinux/greenlinux.jpg"
    ;;
    "xfce")
    # Изменение для XFCE
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/last-image -s "/usr/share/backgrounds/greenlinux/greenlinux.jpg"
    ;;
    *)
    # По умолчанию - изменение для Gnome и других DE
    gsettings set org.gnome.desktop.background picture-uri "file:///usr/share/backgrounds/greenlinux/greenlinux.jpg"
    ;;
    esac
}
changeWallpaper

sudo apt purge -y mintbackup

sudo apt install -y timeshift

sudo dpkg --get-selections > package_list.list

sudo apt install -y language-pack-gnome-ru && language-pack-gnome-ru-base && language-pack-ru && language-pack-ru-base
sudo dpkg-reconfigure locales

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/visual-gl/-/raw/main/supported.d.zip && unzip supported.d.zip && sudo cp -r /tmp/supported.d /var/lib/locales/ && cd ~ && sudo dpkg-reconfigure locales

sudo dpkg --configure -a
sudo apt install -f
sudo apt clean -y
sudo apt autoclean -y
sudo apt autoremove -y

if [ -f /usr/local/bin/green-upgrade ]; then
   sudo rm -r /usr/local/bin/green-upgrade
else
   echo "Продолжаем, осталось совсем немного."
fi

cd /tmp && wget -q https://gitlab.linuxmint.su/greenlinux/green-update/-/raw/upgrade/upgrade.sh && sudo cp -r /tmp/upgrade.sh /usr/local/bin/ && cd ~ && sudo  mv /usr/local/bin/"upgrade.sh" /usr/local/bin/"green-upgrade" && sudo chmod +x /usr/local/bin/green-upgrade
